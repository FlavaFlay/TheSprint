<p style="text-align:center">
<img height="175" width="980" src="../source/images/logo_shadow_half.png">
<br>
<i>A game of high stakes agile software development</i>
</p>

![Version](https://img.shields.io/badge/version-v1.0-00a6b5.svg) ![Players](https://img.shields.io/badge/players-5--12%20people-blue.svg) ![Ages](https://img.shields.io/badge/ages-13+%20years-blue.svg) ![Time](https://img.shields.io/badge/time-15--30%20minutes-blue.svg) ![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg) ![Coverage](https://img.shields.io/badge/coverage-100%25-brightgreen.svg) ![Backers](https://img.shields.io/badge/backers-17%20total-green.svg)

## Contents

- 9 Dexter Character Cards  
_(The Duke, Support Manager, Scrum Lord, Chicken Parm, IntelleWater, Level III Dev, (2) Remote Dev, Billy)_
- 9 Sinister Character Cards  
_(Sniper, Nerlin, Dev Slayer, Village Idiot, Intern, Level II Dev, Max Level Dev, Remote Dev, Billy)_
- 12 Pull Request Cards _(6 Approve Changes & 6 Request Changes Cards)_ 
- 1 Pull Request Map Card
- 12 User Story Cards _(5 OD & 7 OS Cards)_
- 6 Team Tokens
- 24 Planning Poker Tokens _(12 Agreed & 12 Rethrow Tokens)_
- 1 Planning Poker Marker
- 1 Technical Owner Token
- 1 Guido of Boca Raton Token _(Optional)_
- 2 Squad Loyalty Cards _(Optional, for use with Guido of Boca Raton)_
- 8 Allegiance Swap Cards _(Optional, 2 Switch & 6 No Switch Cards)_
- 1 Randy Card _(Optional)_
- 7 User Story Result Markers _(Optional)_

## Objective

The Sprint is a game of hidden squads. Players are either Loyal Dexter fighting for goodness and honor or aligned with the Sinister ways of Nerlin. Dexter wins the game by successfully merging three User Stories. Sinister wins if three User Stories end in failure. Sinister can also win by sniping the Duke at game's end or if a User Story cannot be pointed during Grooming.

Players may make any claims during the game, at any point in the game. Discussion, deception, accusation, and logical deduction are all equally important in order for Dexter to prevail or Sinister to rule the day.

### Game Info

- Ages: 13+
- Players: 5-12
- Play Time: 15-30 minutes

#### Warning

Choking Hazard - Small Parts  
Not for children under 3 years

## The Cards & Tokens

_**Character Cards**_ - Determine the player's squad _(each player is either Dexter or Sinister)_. Character Cards on the side of Dexter have the blue iris logo on blue background and Sinister have a red iris logo on red background. Hereafter references to the _Dexter_ or _Sinister_ squads are denoted with italics. Some characters have special powers during the game - the Duke and the Sniper are included in all games and the remaining special Character Cards are optional. _A player's Character Card may not be revealed at any point in the game, nor the character art discussed (unless a card's rules explicitly permit a reveal)._

_**Technical Owner Token**_ - Designates the player to propose the development Team.

_**Team Tokens**_ - Allocate positions on the development Team.

_**Planning Poker Tokens**_ - Agree or rethrow the Technical Owner's proposed development Team.

_**Planning Poker Marker**_ - Designates the player that represents the last chance to send a development Team on a User Story.

_**User Story Cards**_ - Use the OD set for 5 User Story game variant, OS for the 7 User Story game variant.

_**Pull Request Cards**_ - Determine if a User Story is merged (success) or closed (failure).

_**Pull Request Map Card**_ - Helps organize the field of play by designating holding pens for unused Pull Request cards and zones for playing Pull Request cards. Includes a chart of player counts for each squad on the reverse.

## Set Up

Select the User Story card set for the desired variant (5 or 7 stories). Place the desired User Story cards in the center of the play area with the Team tokens and User Story Result markers (optional). Place the Pull Request cards adjacent to the Pull Request Map card. Give each player a set of two Planning Poker tokens.

Randomly select a Technical Owner (hereafter referred to as the TO); the TO receives the TO token. Use the chart below to determine the number of _Dexter_ and _Sinister_ players. Give the Planning Poker marker to the fifth player to the left of the TO (counting the TO as the first).

| Players | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 |
|:--------|:-:|:-:|:-:|:-:|:-:|:--:|:--:|:--:|
|Dexter   | 3 | 4 | 4 | 5 | 6 | 6  | 7  | 7  |
|Sinister | 2 | 2 | 3 | 3 | 3 | 4  | 4  | 5  |

Shuffle the appropriate number of _Dexter_ Character Cards _(one of these cards will be the Duke card, all the other Dexter Character Cards will be just "Loyal Dexter" cards)_ and _Sinister_ Character Cards _(one of these cards will be the Sniper card, all the other Sinister Character Cards will be just "Sinister Spy" cards)_. Deal one card to each player face down. Each player secretly looks at their Character Card to learn their assigned squad and special powers, if any.  
<img height="350" width="633" src="../source/images/setup-example.png">  
_Example set up for a 6 player game with the TO starting in the bottom right._

_**Note:**_ Many Character Cards have special powers or abilities. Prior to the start of the game, the players may elect to ignore any included character's special powers and treat that character as a default _"Loyal Dexter"_ or _"Sinister Spy"_ depending on the character's squad membership.

### Sinister reveals itself, but the Duke sees the truth

_Sinister is rampant in the dev shop. Agile represents the future of software, a promise of productivity, yet hidden among the eager engineers are Nerlin's devious developers. These forces of Sinister are few in number but have knowledge of each other and remain hidden from all but one of the engineers. The Duke alone knows the agents of Sinister, but he must carefully hint at the truth. If his true identity is discovered all will be lost._

After all the players know their squad, the TO must ensure that all the _Sinister_ players know each other and the Duke knows all the _Sinister_ players by repeating the following script:

>_"Everyone have your hand extended and close your eyes"_
>
>_"Sinister Spies open your eyes, lift your thumbs, and look around to identify your fellow squad mates"_
>
>_"Sinister Spies, close your eyes and lower your thumbs"_
>
>_"Sinister Spies, lift your thumbs"_
>
>_"The Duke, open your eyes and make note of the Sinister Spies. Use your knowledge carefully to prevent them from closing too many Pull Requests"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Everyone wake up"_

## Game Play

The game consists of several User Stories; each User Story has a Grooming phase and a Review phase. In the Grooming phase the TO proposes a Team to complete a User Story - via Planning Poker all the players will either agree to the proposed Team and move to the Review phase, or reject the proposed Team passing technical ownership to the next player and repeating the process until a Team is approved. In the Review phase those players selected to be on the Team will determine if the Pull Request is merged (successful).

Choose your Team wisely. Approve only Teams where you trust everyone. Even a single _Sinister_ player on the Team is enough for failure.

### Grooming Phase

_It is time for great decisions and strong technical owners. Not all developers agree about the meaning of Agile, and yet you must choose only those that are Dexter to faithfully represent Agile in the User Stories. If an open ear and eye are kept, the Duke's sage advice can be discerned as whispers of truth._

_**Team Assignment:**_ After appropriate discussion, the TO takes the required number of Team Tokens _(using the following chart, or numbers on the User Story cards)_ and assigns each Team Token to any player.  
<img height="375" width="603" src="../source/images/teamtoken.png">  
_The TO must judiciously distribute the instruments of software development._  

#### 5 User Story Team Size
|Players          | 5 | 6 | 7 | 8 | 9 | 10 |
|:----------------|:-:|:-:|:-:|:-:|:-:|:--:|
|1st User Story   | 2 | 2 | 2 | 3 | 3 | 3  |
|2nd User Story   | 3 | 3 | 3 | 4 | 4 | 4  |
|3rd User Story   | 2 | 4 | 3 | 4 | 4 | 4  |
|4th User Story*  | 3 | 3 | 4 | 5 | 5 | 5  |
|5th User Story   | 3 | 4 | 4 | 5 | 5 | 5  |

#### 7 User Story Team Size
|Players          | 7 | 8 | 9 | 10 | 11 | 12 |
|:----------------|:-:|:-:|:-:|:--:|:--:|:--:|
|1st User Story   | 3 | 3 | 3 | 3  | 3  | 3  |
|2nd User Story   | 4 | 4 | 4 | 4  | 4  | 4  |
|3rd User Story   | 3 | 5 | 4 | 4  | 5  | 6  |
|4th User Story** | 4 | 4 | 4 | 5  | 5  | 6  |
|5th User Story   | 3 | 4 | 4 | 4  | 5  | 5  |
|6th User Story*  | 4 | 5 | 5 | 5  | 6  | 6  |
|7th User Story   | 4 | 5 | 5 | 5  | 6  | 6  |

The TO can be on the Team, but is not required to be so. A player may only be assigned one Team Token.

_Discuss, debate, then discuss some more! All the players should participate in helping the TO make the right choice of players to be on the Team. Active and logical discussion is a great way to catch Nerlin's agents in their webs of deceit._

_**Planning Poker:**_ After appropriate discussion, the TO calls for a vote on the Team assignments.

_The TO is proposing the Team - but all the players have a say in whether to accept or reject the proposal. The TO can be Sinister, or one of the players chosen could be a mistake. Don't feel that you need to accept all the proposed Teams. If you reject the Team then a new TO can propose a different Team, maybe one without any Sinister players on it._

Each player, including the TO, secretly selects one Planning Poker token. When all players have their selected token ready, the TO asks for the tokens to be revealed. All tokens are flipped over so everyone can see how you voted. The Team is approved if the majority accepts. If the selected Team is agreed upon, play continues in the Review phase _(below)_. If the selected Team is rethrown _(a tied Vote is also rethrow)_, the TO passes clockwise and the Grooming phase is repeated.

_Sinister_ wins the game if five Teams are rethrown in a single round _(5 consecutive failed Votes, i.e. failure to point a User Story)_. The do-or-die Grooming session is indicated by the Planning Poker marker. When the same player possesses the TO token and the marker, if that player's development Team is rethrown, _Sinister_ wins.

#### Strategy Tip: Everyone's a suspect

If you aren't confident of everyone on the proposed Team, then you should strongly consider rejecting the proposal. Rejecting a Team is not a sign that you are _Sinister_. A group of skilled players will usually do the Grooming phase three or more times before agreeing to a Team. Watch who approves, and ask them why they approved - sometimes _Sinister_ players will approve because they know another _Sinister_ player was included. The Duke can also use his voting as a signal, but be careful the _Sinister_ players will be watching.

### Review Phase

_You have debated well and wisely chosen the brave engineers with whom you place your trust. Now it is time to measure a person's true intent and loyalty to the noble cause of Agile. Be true and Dexter will prevail._

The TO passes a set of Pull Request cards to each Team member and sets the current User Story card in front of the 'Review Requested' icon on the Pull Request Map card. Each player on the User Story selects a Pull Request card and plays it face down on top of the current User Story card. Unselected cards are discarded to the pile marked by the trash can icon. The TO collects and shuffles the played Pull Request cards before revealing. The User Story is completed successfully with a merged pull request only if all the cards revealed are 'approve changes' cards. The User Story fails with a closed pull request if one _(or more)_ 'request changes' cards have been played.

_**Note:** The Dexter players must select the 'approve changes' card; Sinister may select either the 'approve changes' or 'request changes' card._

_**Note:** The 4th User Story (the 6th in 7 User Story variant) in games of 7 or more players requires at least two 'request changes' cards to be a closed pull request and failed User Story._

_**Note:** The 4th User Story in the 7 User Story variant requires each Sinister player working on the User Story to cast a 'request changes' card. Exactly one 'request changes' card results in a closed pull request and failed User Story. Any other result is a successfully completed and merged User Story._

_**Note:** It's suggested that two different players shuffle the played and discarded Pull Request cards before revealing._

_**Note:** It's best to designate a player not on the Team to collect all the discarded Pull Request cards so that it's clear which cards are played and which are discarded. Shuffle the discarded Pull Request cards._

Indicate a successfully merged User Story by flipping over the User Story card, revealing the blue _Dexter_ logo, and covering the rest of the card with the next User Story. Otherwise, a failed and closed User Story is shown by revealing the red _Sinister_ logo. The optional User Story Result markers may instead be used to indicate the results rather than flipping cards. The TO passes clockwise, the Planning Poker marker is set to the fifth player to the left of the TO, and the next User Story begins in the Grooming phase.  
<img height="208" width="600" src="../source/images/mark-result.png">  
_Examples of the two styles of tracking User Story results._  

## Game End

_Dexter and goodness prevail if the team is able to successfully merge three User Stories without revealing the Duke's true identity. Nerlin's dark forces of Sinister triumph when three User Stories end in failure, or are devious enough to force the Duke into the open._

The game ends immediately after either three successfully merged or three closed User Stories. The _Sinister_ players win if three User Stories close. The game also ends immediately and the _Sinister_ players win if five Teams are rethrown in a single round _(5 consecutive failed Votes, i.e. failure to point a User Story)_.

_**Note:** The 7 User Story variant takes four successful merges or four closes to end the game._

### Snipe the Duke - Sinister's Last Chance

If three User Stories are merged successfully, the _Sinister_ players will have a final opportunity to win the game by correctly naming which of the _Dexter_ players is the Duke. With only the Sniper revealing his Character Card, the _Sinister_ players discuss and the player with the Sniper Character Card will name one _Dexter_ player as the Duke. If the named player is the Duke, then the _Sinister_ players win. If the _Sinister_ players do not name the Duke then the _Dexter_ players win.

## Options & Expansions

### Optional Character Cards with special powers

Several additional characters with specials powers are available to play. You can play with these cards in any combination that you would like. Different combinations will make the game harder to win for one side of the battle or the other. It is best to add one special Character Card into a game at a time. Once familiar with how they play, more special characters may be added or changed. In most cases you will want to play with the Duke, but it is not required.

_**Support Manager**_ is an optional character on the side of _Dexter_. Support Manager's special power is knowledge of the Duke at the start of the game. Using his knowledge wisely is key to protecting the Duke's identity. Adding Support Manager into a game will make the _Dexter_ side more powerful and win more often.  
_**Note:** For games of 5, be sure to add either Nerlin or Dev Slayer when playing with Support Manager._

_**Nerlin**_ is an optional character on the side of _Sinister_. Nerlin's special power is that his identity is not revealed to the Duke at the start of the game. Adding Nerlin into a game will make the _Sinister_ side more powerful and win more often.

_**Dev Slayer**_ is an optional character on the side of _Sinister_. Dev Slayer's special power is that she appears to be the Duke - revealing herself to Support Manager as the Duke. Adding Dev Slayer into a game will make the _Sinister_ side more powerful and win more often.

_**Max Level Dev**_ is an optional character on the side of _Sinister_. The Max Level Dev _must always_ request changes when put on a User Story. _Option_: As an additional rule mechanic, the Max Level Dev can force a closed pull request on a User Story _if_ the following conditions are met: 1) he was placed on the User Story, 2) the result of the User Story is a merged pull request, and 3) he reveals his Character Card.

_**Intern**_ is an optional character on the side of _Sinister_. Intern's special power is that he is known only to his mentor Nerlin, and knows only his mentor Nerlin at the start of the game. Without Nerlin in the game, the Intern is truly lost! Adding Intern into a game will make the _Dexter_ side more powerful and win more often.  
_**Note:** With Intern and Nerlin, the game is best with at least 4 total Sinister roles._

_**Village Idiot**_ is an optional character on the side of _Sinister_ with a unique win condition. Village Idiot's special power is that he does not reveal himself to the other _Sinister_ players, nor does he gain knowledge of the other _Sinister_ players at the start of the game. He does not open his eyes during the "_Sinister Spy_" reveal at the start of the game, although he is allowed to 'request changes' on Pull Requests. Village Idiot wins if and only if the game goes to the final User Story and _Dexter_ wins. Whether _Sinister_ snipes the Duke or not, Village Idiot wins if _Dexter_ wins on the final User Story. Adding Village Idiot into a game will make the _Dexter_ side more powerful and win more often. 

_**Chicken Parm**_ is an optional character on the side of _Dexter_. Chicken Parm has the unfortunate special ability of having to show a _Sinister_ squad card when Guido is used on him. Adding Chicken Parm to a game can make the _Sinister_ side more powerful and win more often.

_**IntelleWater**_, on the side of _Dexter_, has a special ability that does not trigger until expansion packs.

_**Scrum Lord**_ is an optional character on the side of _Dexter_. Scrum Lord's special powers are ceremony interrupts, of which there are five. To activate an interrupt, the Scrum Lord must reveal his Character Card. Once revealed, the Scrum Lord may not use any more abilities. The five interrupts are:

- _Reflection:_ when hit with Guido, forces the player passing Guido to have to reveal his/her squad to the Scrum Lord. The player then must immediately pass Guido to a new player, but does not get to check the squad of that player.
- _Inspection:_ when using Guido, forces the player receiving Guido to reveal his/her Character Card to the Scrum Lord.
- _Nullification:_ may overrule the Max Level Dev if that character reveals his card to force a closed pull request.
- _Usurpation:_ may take over the TO role and select a Team that is auto-approved.
- _Time Box:_ at game's end when _Sinister_ is trying to Snipe the Duke, forces the Sniper to act three seconds after the Scrum Lord reveal. In the spirit of agile ceremonies, _Sinister_ should be permitted some time to discuss.

_**Remote Dev**_ is a character class that marks three roles, two for _Dexter_ and one for _Sinister_. During the reveal phase, all Remote Devs will identify each other. At the end of the game, if _Dexter_ successfully merged three User Stories, the _Sinister_ Remote Dev wins if the Sniper chooses one of the _Dexter_ Remote Devs and loses if the Sniper chooses any other character to snipe, even the Duke.

The reveal phase at the start of the game will vary depending on which roles are added - see below for new scripts to use for the different Character Cards that are included.

>_"Everyone have your hand extended and close your eyes"_
>
>_"Sinister Spies, not Village Idiot or Intern, open your eyes, lift your thumbs, and look around to identify your fellow squad mates"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Intern, lift your thumb. Nerlin, open your eyes. Nerlin, it is your responsibility to instill in the Intern your Sinister ways"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Nerlin, lift your thumb. Intern, open your eyes. Intern, look up to your mentor Nerlin as the true source of Sinister ways"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"All Sinister Spies except Nerlin, lift your thumb so that the Duke may know you. Village Idiot and Intern must also lift their thumbs"_
>
>_"The Duke, open your eyes and see the agents of Sinister. Use your knowledge carefully to prevent them from closing too many Pull Requests. Beware, for Nerlin may be lurking amongst the Dexter"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Support Manager, open your eyes"_
>
>_"The Duke & Dev Slayer, lift your thumb so that Support Manager may identify you. Support Manager, may you place your trust wisely, for the fate of Dexter depends on it"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Remote Devs, open your eyes and lift your thumbs. There should be exactly three of you. If you are Dexter, you are looking at exactly one Dexter and one Sinister dev. If you are Sinister, you are looking at two Dexter devs who are not the Duke"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"Everyone wake up"_

### Optional Rules

_**Targeting:**_ The targeting variant allows the players to complete the User Stories in any order they see fit adding a level of strategic planning into the game.

During the Grooming phase, the TO chooses both which players to be on the Team and which User Story the Team will attempt to complete. The number of players chosen to be on the Team must correspond to the number required for that User Story. After the Review phase, flip the attempted User Story card. Once attempted, a User Story may not be attempted a second time.

The last User Story cannot be attempted until at least two (three in 7 User Story variant) other User Stories have been successfully merged.

_**Guido of Boca Raton & Squad Loyalty Cards:**_ The Guido of Boca Raton token is an optional player ability. The player with Guido of Boca Raton will be able to look at the squad of another player. Unlike the other character powers, the player that has this ability is open information.

At the beginning of the game, give the Guido of Boca Raton token to the player on the TO's right. Immediately after the 2nd, 3rd, and 4th User Stories are resolved (2nd, 3rd, 5th, 6th in 7 User Story variant), the player with the Guido of Boca Raton token will choose one player to examine. The player being examined will receive the pair of Squad Loyalty Cards and pass the card that corresponds to the squad of their Character Card to the Guido of Boca Raton. Using the wrong Squad Loyalty Card will result in losing the game.

The Guido of Boca Raton may discuss, but cannot reveal the Squad Loyalty Card passed.

The player being examined receives the Guido of Boca Raton token. A player that used Guido of Boca Raton cannot have Guido used on them.

_**Note:** Guido of Boca Raton is best saved for games of 7 or more people. Adding Guido into a game will make the Dexter side more powerful and win more often._

### Billy

A complex character with many underlying motivations, Billy can be driven towards either _Dexter_ or _Sinister_.

#### Variant #1

##### Billy switches allegiance during the game

###### Set Up
- Use both Billy Character Cards in place of one _Dexter_ and one _Sinister_ Character Card.
- Build an Allegiance Swap deck of 5 cards – three 'No Change' (blank) cards and two 'Switch Allegiance' cards. For the 7 User Story variant, add one more 'No Change' card.
- Shuffle and place the Allegiance Swap deck adjacent to the User Stories.

###### Reveal Phase
The Sinister Billy _does not_ open his eyes, but instead lifts his thumb so that he is known to the other agents of _Sinister_. The Duke also gets to know the starting _Sinister_ Billy.

###### Play
Game play proceeds as normal, with some exceptions. At the beginning of the 3rd round (4th round for 7 User Story variant) and at the very start of each subsequent round flip one card from the Allegiance Swap deck.

- If the card is 'No Change', there is no change of allegiance and play continues as normal.
- If the 'Switch Allegiance' card is drawn, Billy has had a change of heart. The two Billy players secretly switch their allegiances. This switch applies to all aspects of game play including victory conditions and rules regarding playing Pull Request cards. They do not swap or show their Character Cards.
- When playing with Guido, Guido may request to know a player's squad or if he is one of the two Billies, but not both.

It is possible Billy will switch allegiance once, twice or even not at all during a game.

#### Variant #2

##### Billy's allegiance switches are known in advance

###### Set Up
- Use both Billy Character Cards in place of one _Dexter_ and one _Sinister_ Character Card.
- Build an Allegiance Swap deck of 7 cards – five 'No Change' (blank) cards and two 'Switch Allegiance' cards. For the 7 User Story variant, add one more 'No Change' card.
- Shuffle and place the Allegiance Swap deck adjacent to the User Stories.

###### Reveal Phase
The Sinister Billy _does not_ open his eyes, but instead lifts his thumb so that he is known to the other agents of _Sinister_. The Duke also gets to know the starting _Sinister_ Billy.

###### Play
Game play proceeds as normal with some exceptions.

- At the beginning of the game deal, face-up, one allegiance card per User Story. These cards will indicate if and when Billy will switch allegiance during the game. A switch of allegiance takes effect at the beginning of the stories with a 'Switch Allegiance' card attached to it.
- Options on playing Pull Request cards are eliminated. The player that is the current _Sinister_ Billy must play a 'request changes' card. The player that is the current _Dexter_ Billy must play the 'approve changes' card.
- When the 'Switch Allegiance' card takes effect, Billy has had a change of heart. The two Billy players secretly switch their allegiances at the beginning of the User Story. This switch applies to all aspects of game play including victory conditions and rules regarding playing Pull Request cards. They do not swap or show their Character Cards.
- When playing with Guido, Guido may request to know a player's squad or if he is one of the two Billies, but not both.

It is possible Billy will switch allegiance once, twice, or not at all during a game.

#### Variant #3

##### The Billies know each other
(Recommended for larger groups only.)

###### Set Up
- Use both Billy Character Cards in place of one _Dexter_ and one _Sinister_ card.

###### Reveal Phase
Add a step to the end of the script as follows:
>_"Billies open your eyes and lift your thumbs to identify your counterpart"_
>
>_"Billies close your eyes and lower your thumbs"_

#### Variant #4

##### One Billy of unknown allegiance joins the fray

###### Set Up
- One of the designated _Sinister_ Character Cards must be the Village Idiot.
- Secretly shuffle the two Billies together face down.
- In place of one _Dexter_ Character Card, insert one Billy into the Character Card deck without revealing which Billy is in play.

###### Reveal Phase
- If a player receives the _Sinister_ Billy, he will act as a default _"Sinister Spy"_ knowing the _Sinister_ squad and revealing himself to the Duke.
- If a player receives the _Dexter_ Billy, he will act as a default _"Loyal Dexter"_.

### Randy

_He who is touched by the spirit of Randy has tremendous, yet ultimately chaotic, power. Choose wisely and use cautiously lest the forces of Sinister be strengthened._

Randy can be added to the game as an optional player power. Randy gives a member of the development Team the ability to alter the outcome of the User Story by switching another player's played Pull Request card.

During the Grooming phase, Randy is assigned by the TO to one of the players on the Team by giving that player both a Team token and the Randy card. The TO _cannot_ assign Randy to himself.

During the Review phase, Team members select their Pull Request card and play it face down in front of themselves so that it is clear which player played what card. Before the team moves their played Pull Request cards to the User Story, the player with Randy _may_ tell any one other player to switch their Pull Request cards (the unplayed card becomes the played card).

After switching the cards, the player with Randy looks at the switched card (the originally played card) before it moves to the discard pile. He thus knows which card that player chose, and if his switch was beneficial or harmful to the User Story. The TO then collects and shuffles the played Pull Request cards before revealing as normal.

Randy does not get to see a player's Pull Request card unless he decides to switch that player's card.

## Credits

_**Game Design:**_ Robert "Chicken Parm" Goldschmidt

_**Game Development:**_ Robert "Chicken Parm" Goldschmidt

_**Graphic Design/Illustration:**_ Robert "Chicken Parm" Goldschmidt

_**Acknowledgments:**_ Our company for being a great place to work. GitHub for providing an awesome icon pack (octicons.github.com (c) 2018 GitHub Inc.) for which permission is granted via the MIT License. BoardGameGeek (boardgamegeek.com) for being an awesome resource and community, especially users tubbytwins (7 User Story variant) and sdotco (minimalist retheme). Last but certainly not least, Indie Boards and Cards (indieboardsandcards.com) for bringing to life such an amazing game in Don Eskridge's The Resistance: Avalon, designed and developed by Don Eskridge and Travis Worthington respectively.

### Backstory 
The characters depicted have day jobs (and sometimes nights and weekends) in the medical software industry creating solutions for eye doctors. In real life the developers are actually divided into two squads named Dexter (oculus dexter, or OD, for the right eye) and Sinister (oculus sinister, or OS, for the left eye). The software project abbreviations in Jira are even OD and OS respectively. Not only are the developers for each squad fully represented in the artwork for the game, but character special powers are remarkably relevant to job titles and real-world office events. Obviously, the developers are addicted to this game.
