## The Micromanagement Expansion Pack!

_The Executive Leadership Team is not pleased with the product delays exacerbated by the actions of Sinister. Not pleased at all. In an effort to move things forward, they have instructed management to, quote unquote, get their fingers all up in there._

### Contents

- 16 Management Cards 
- 2 Pull Request Cards _(1 Approve Changes Card & 1 Request Changes Card)_ 

### Game Play

When selecting a team for User Stories 1-4, the Technical Owner also chooses a person _not_ on the story to receive one card from the management deck. If the team is approved, that person gets the option to draw from the shuffled management deck to earn the powers of management. A player may only have one management card at a time. If designated as management and electing to draw from the deck, a player must reveal and discard the card currently in their possession before drawing.

Any number of managers may insert themselves into the development process per User Story. Most managers may only be played between the end of the Grooming Phase and the start of the Review Phase, so please pause during this transition to allow managers a chance to exercise their powers. All management cards remain secret until revealed by playing or discarding.

_**Standard:**_ Use the entire set of managers to form a sixteen card management deck.

_**Option A:**_ Just as not all companies have a full suite of executives and management, it is possible (and encouraged!) to play with a subset of the management deck. The players collectively agree on which managers to include and exclude from the game before dealing the Character Cards.

_**Option B:**_ Players can secretly add managers to the management deck to create a subset of the management deck with which to play. Take all the management cards after dealing the Character Cards. Automatically add the CEO and one HR card to the management deck. The player to the right of the Technical Owner takes the remaining cards and chooses one to add to the management deck. Without revealing them, two cards are randomly banished from the remaining cards. If there are fewer than 8 players, banish one more card. Moving counterclockwise, each subsequent player adds a single card to the management deck from the remaining cards, ending at the player to the left of the Technical Owner. The starting Technical Owner may not add a manager to the management deck nor see the remaining cards. Shuffle and play with the resulting management deck!

### Rule Clarifications

_Sales Rep_ may not be played on the third User Story if the first two User Stories ended in favor of _Dexter_.