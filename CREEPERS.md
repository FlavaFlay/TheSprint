## Creeper Expansion Pack!

The software is taking over the market, but now the company wants to expand into new markets. An increasing demand on the research and development department can mean only one thing. It's time to hire new developers and form a new squad, the _Creepers_!

There is one challenge with new developers. While they may be talented, they simply do not have the depth of knowledge on a mature product to avoid making breaking changes to the code...

### Contents
- 5 Creeper Character Cards _(QA, Level III Dev, Level II Dev, Level I Dev, No Scuffs)_
- 6 'Buggy Code' Pull Request Cards
- 1 Pull Request Map Card
- 5 'OU' User Story Cards
- 1 Creeper Loyalty Card _(Optional, for use with Guido of Boca Raton)_
- 5 User Story Result Markers

### The Dynamic Has Changed!
_The Creepers have been unleashed on the dev shop. They are completing tickets, but they break code in far-flung reaches that only the most tenured developers can foresee. In the name of preserving software quality, it is actually a good thing to request changes on their Pull Requests!_

During the Review Phase of the game, each player on the agreed development Team will receive _three_ different result cards, the 'approve changes' card, 'request changes' card, and 'buggy code' card. _Dexter_ must always 'approve changes'. _Sinister_ may 'approve changes' or 'request changes'. _Creepers_ may 'approve changes' or indicate that the Pull Request contains 'buggy code'.

User Stories that merge become a victory for _Sinister_ if a 'buggy code' card is present in the results. User Stories that close become a victory for _Dexter_ if a 'buggy code' card is present in the results. All other User Story outcome mechanics are otherwise the same as the base game.

With a few exceptions, _Creepers_ win the game when _Dexter_ wins.

### Set Up
Follow the normal set up process. Use the 'OU' User Story card set. Place this expansion's Pull Request Map card in the play area. Stack the 'buggy code' Pull Request cards adjacent to the bug icon of the map card.

Use the chart below to determine the number of _Dexter_, _Sinister_, and _Creeper_ players.

|Players   | 7 | 8 | 9 | 10 | 11 | 12 |
|:---------|:-:|:-:|:-:|:--:|:--:|:--:|
|Dexter    | 3 | 4 | 4 | 5  | 6  | 6  |
|Sinister  | 2 | 2 | 3 | 3  | 3  | 4  |
|Creeper   | 2 | 2 | 2 | 2  | 2  | 2  |

Shuffle the appropriate number of _Dexter_, _Sinister_, and _Creeper_ Character Cards. Deal one card to each player face down. Each player secretly looks at their Character Card to learn their assigned loyalty and special powers, if any.

#### Creeper Characters and special abilities
All _Creepers_ have a general set of rules that govern how they must act during the game (unless their specific Character Card has an overriding rule).

- Whenever a _Creeper_ is designated as the TO, he _must_ put himself on the User Story. If the team is subsequently agreed, he must then play the 'buggy code' card during Pull Request Review.
- Whenever a _Creeper_ is put on the approved development Team, he has the option to 'approve changes' or declare 'buggy code'.
- During the reveal phase, all _Creepers_ will learn of their fellow _Creepers_.
- _Creepers_ may never exercise the power of Randy.

_**QA**_ is a _Clairvoyant Creeper_. He knows the same set of Spies as the Duke, knowing all of _Sinister_ but Nerlin.

_**Level III Dev**_ is a _Careful Creeper_. He is so good at coding that he is not required to play the 'buggy code' card when on a team that he puts together as TO.

_**Level II Dev**_ is a _Confident Creeper_. As a prolific creator of Pull Requests, he must always play the 'buggy code' card when on a User Story if neither _Dexter_ nor _Sinister_ has more than one victory. He is therefore required to declare ‘buggy code’ if he is on the 1st or 2nd User Stories, and may be forced to declare so on the 3rd User Story.

_**IntelleWater**_ is a _Dexter_ role from the base game. As an honorary _Creeper_, IntelleWater will get to learn of the _Creepers_ during the reveal phase. When in control of Guido of Boca Raton, IntelleWater must target a _Creeper_ if any are valid targets. _Option:_ As an adjusted rule mechanic, IntelleWater can simply open his eyes at the same time as the rest of the _Creepers_. In this case, the _Creepers_ themselves are uncertain which of the two people they see is also a _Creeper_, although IntelleWater will know exactly who the two _Creepers_ are.

The reveal phase at the start of the game will vary depending on which roles are added - see below for new scripts to add for the different Character Cards that are included.

>_"Everyone have your hand extended and close your eyes"_
>	
>_"All Sinister Spies except Nerlin, lift your thumb so that QA may know you. Village Idiot and Intern must also lift their thumbs"_
>	
>_"QA, open your eyes and see the agents of Sinister. Use your knowledge carefully to maneuver their change requests to target buggy code. Beware, for Nerlin may be lurking amongst the Dexter"_
>
>_"Close your eyes and lower your thumbs"_
>
>_"All Creepers open your eyes and lift your thumbs. Bashfully commiserate as the potential sources of buggy code"_
>	
>_"Close your eyes and lower your thumbs"_
>	
>_"All Creepers lift your thumbs"_
>	
>_"IntelleWater, open your eyes and gaze upon the potential sources of buggy code. But they are pretty good devs, so that's probably not going to happen"_
>	
>_"Close your eyes and lower your thumbs"_
>	
>_"Everyone wake up"_
	
### Game Play

#### Grooming Phase

The TO must select a team with the correct number of players as indicated by the chart below. If the TO is a Creeper, he _must_ put himself on the User Story.

##### Team Size
|Players          | 7 | 8 | 9 | 10 | 11 | 12 |
|:----------------|:-:|:-:|:-:|:--:|:--:|:--:|
|1st User Story   | 3 | 3 | 3 | 4  | 4  | 4  |
|2nd User Story   | 4 | 4 | 4 | 5  | 5  | 5  |
|3rd User Story   | 3 | 5 | 4 | 5  | 5  | 5  |
|4th User Story*  | 4 | 4 | 5 | 6  | 6  | 6  |
|5th User Story   | 4 | 5 | 5 | 6  | 6  | 6  |

#### Review Phase

Each player selected for the development Team gets a set of the three possible Pull Request Cards. After shuffling the three cards, _Dexter_ and _Sinister_ immediately discard the 'buggy code' card, and _Creepers_ immediately discard the 'request changes' card. _Creepers_ may then select either the 'approve changes' card or the 'buggy code' card. If the TO is a _Creeper_, he _must_ select the 'buggy code' card. _Dexter_ and _Sinister_ play as before. Each player puts their selected card on top of the current User Story card, and the unselected card goes to the discard pile. Then the TO collects, shuffles, and reveals the result.

_**Note:** The 4th User Story in games of 9 or more players requires at least two 'request changes' cards to be a closed pull request._

To determine the outcome of a User Story, first put aside any 'buggy code' cards. Determine if the remaining result cards would have caused a closed or merged User Story. If a User Story without 'buggy code' is merged, display a _Dexter_ merged marker. If a User Story with 'buggy code' is merged, display a _Sinister_ merged marker. If a User Story without 'buggy code' is closed, display a _Sinister_ closed marker. If a User Story with 'buggy code' is closed, display a _Dexter_ closed marker.

### Game End

The game ends immediately after either _Dexter_ or _Sinister_ takes three User Stories. The _Sinister_ players win by taking three User Stories. The game also ends immediately and the _Sinister_ players win if five Teams are rethrown in a single round (5 consecutive failed Votes, i.e. failure to point a User Story).

If _Dexter_ takes three User Stories and more than 50% of User Stories over the course of the game had buggy code, _Creepers_ win. _Dexter_ or _Sinister_ may still also win at this point, depending on the outcome of sniping. In other words, if the game ends in _Dexter's_ favor on the 3rd User Story, _Creepers_ automatically win if two or more User Stories had buggy code. On the 4th or 5th User Story, _Creepers_ automatically win if three or more User Stories had buggy code.

#### Sniping - Sinister's Last Chance

If _Dexter_ takes three User Stories, the _Sinister_ players will have a final opportunity to win the game. If 50% or fewer User Stories had buggy code, sniping either the Duke or QA will mean _Sinister_ wins and _Dexter_ and _Creepers_ lose. If more than 50% of User Stories had buggy code, sniping the Duke will mean _Sinister_ wins with _Creepers_ but _Dexter_ loses. If the Sniper does not make a game winning snipe, _Dexter_ and _Creepers_ win while _Sinister_ loses.

### Interactions with Base Game Explained

#### Randy
When Randy is in play, each player makes their immediate discard after picking up the three Pull Request cards, as described in the Review Phase. Each player then selects the card intended for play in the User Story between their two remaining cards.

If Randy is used on a player, the player who exercised Randy gets to see the selected card, then move it to the discard pile. The targeted player then places their intended discard on the User Story card. All other players play their originally selected card on the User Story and discard their unplayed card.

If a _Creeper_ is bestowed with the power of Randy, he is not permitted to use it when the time comes. He must decline to exercise the power of Randy.

#### IntelleWater with Guido of Boca Raton
If IntelleWater is in possession of Guido, when a _Creeper_ is a valid target for Guido, IntelleWater must use Guido on a _Creeper_. If no _Creepers_ are valid targets, then and only then may IntelleWater freely target any player.

#### Max Level Dev
The _Sinister_ Max Level Dev’s optional ability can only force a closed pull request. If a closed pull request contains buggy code, that User Story will still be a victory for _Dexter_.

### Backstory

As _The Sprint_ was being developed, a third squad was actually being formed to grow and enhance the existing medical software products for eye doctors. This squad was populated by developers new to the company. In real life the name of the squad is Honeycreepers, but it's easier to call them Creepers. The abbreviation for their project in Jira is OU (oculus uterque, for both eyes).