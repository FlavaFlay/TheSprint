## README.md

### Teaser
The Sprint pits the forces of Dexter and Sinister in 
a battle to meet deadlines and feature commitments. 
The Technical Owner is the earnest manager trying to 
guide the squad to code completion and meeting the 
definition-of-done. Hidden among his eager engineers 
are Nerlin's devious developers who take a different 
view of agile software development methodologies. 
These forces of Sinister are few in number but have 
knowledge of each other and remain hidden from all 
but one of the Technical Owner's engineers. The Duke 
alone knows the agents of Sinister, but he must 
carefully hint at the truth. If his identity is 
discovered all will be lost.

Will Dexter prevail?  
Or will the Sprint  
fall under Nerlin's  
dark shadow?

Check it out!  
Literally.  
With Git.  

... Or maybe clone it.

### Game Info
- Ages: 13+
- Players: 5-12
- Play Time: 15-30 minutes

### Warning
Choking Hazard - Small Parts  
Not for children under 3 years